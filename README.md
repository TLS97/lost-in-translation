# Sign Language Translator

## Description

## Install

To run the project, first install the dependencies:

```
npm install
```

## Usage

Start the application by running the following command:

```
npm start
```

## Contributors

[Ha Hoang](https://gitlab.com/hhoan)
[Tine Storvoll](https://gitlab.com/TLS97)
